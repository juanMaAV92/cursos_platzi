
package POO;

class Main {
    
    public static void main(String[] args) {
        UberX uberX = new UberX( "XXX111", new Account( "JuanMa", "1111111111" ), "chevrolet", "sonic");
        uberX.setPassenger(4);
        uberX.printDataCar();
        
        UberVan uberVan = new UberVan( "XXX222", new Account( "JuanMa", "222222222" ));
        uberVan.setPassenger(6);
        uberVan.printDataCar();
    }

}