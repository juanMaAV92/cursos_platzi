package POO;

public class Car {

    private Integer id;
    private String license;
    private Account driver;
    private Integer passenger;
  

    public Car( String license, Account driver){
        this.license = license;
        this.driver = driver;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPassenger() {
        return passenger;
    }

    public void setPassenger(Integer passenger) {
        if(passenger == 4){
            this.passenger = passenger;
            return;
        }
        System.out.println("Necesitas asignar cuatro pasajeros");
    }

    void printDataCar(){
        System.out.println( "License: " + license + " Name Driver: " + driver.name  );
    }
}
