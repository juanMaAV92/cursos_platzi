
- Extensiones necesarios VSCode
    -   [PHP Server](https://marketplace.visualstudio.com/items?itemName=brapifra.phpserver)
    -   [PHP Debug](https://marketplace.visualstudio.com/items?itemName=xdebug.php-debug)
    -   [PHP Extension Pack](https://marketplace.visualstudio.com/items?itemName=xdebug.php-pack)

- Instalar Xampp

- Crear variable de entorno 
    -   Dentro la  variable PATH agregamos la Ruta __C:\xampp\php__

- Ejecutar servidor integrado en php 

```php
php -S localhost:8080 index.php
```