
<?php
    require_once('Car.php');
    require_once('Account.php');
    require_once("UberX.php");
    require_once("UberVan.php");
    require_once("UberPool.php");


    $uberX = new UberX("XXX111", new Account("JuanMa", "111111"), "Chevrolet", "Spark");
    $uberX->setPassenger(4);
    $uberX->printDataCar();
    
    $uberPool = new UberPool("XXX222", new Account("JuanMa", "2222222"), "Mazda", "6");
    $uberPool->setPassenger(4);
    $uberPool->printDataCar();

    $uberVan = new UberVan("OJL395", new Account("Raúl Ramírez", "AND456"), "Nissan", "Versa");
    $uberVan->setPassenger(6);
    $uberVan->printDataCar();
?>  